package geeksdobyte.com.animefreakz

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ImageView
import android.widget.TextView
import java.util.*

/**
 * Created by rkwork on 10/25/17.
 */
class gridAdapter(private val getContext: Context, private val gridLayoutId: Int, private val gridItem: ArrayList<gridLayout>) : ArrayAdapter<gridLayout>(getContext, gridLayoutId, gridItem) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


        var row = convertView

        val Holder: ViewHolder



        if (row == null) {
            val inflater = (getContext as Activity).layoutInflater

            row = inflater!!.inflate(gridLayoutId, parent, false)

            Holder = ViewHolder()

            Holder.img = row!!.findViewById(R.id.img)

            Holder.txt = row.findViewById<TextView>(R.id.txt)

            row.tag = Holder
        } else {
            Holder = row.tag as ViewHolder
        }

        val item = gridItem[position]

        Holder.img!!.setImageResource(item.image)
        Holder.txt!!.text = item.text

        return row

    }

    class ViewHolder {
        internal var img: ImageView? = null
        internal var txt: TextView? = null

    }
}