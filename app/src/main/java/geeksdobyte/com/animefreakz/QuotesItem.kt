package geeksdobyte.com.animefreakz
data class QuotesItem(val quote: String = "",
                      val name: String = "",
                      val anime: String = "")