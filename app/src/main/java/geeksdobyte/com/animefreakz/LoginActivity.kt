package geeksdobyte.com.animefreakz


import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth


class LoginActivity : AppCompatActivity() {
    var fbAuth = FirebaseAuth.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        var passbox: EditText = findViewById<EditText>(R.id.passwordBox)
        var userbox: EditText = findViewById<EditText>(R.id.usernameBox)
        var btnLogin: Button = findViewById<Button>(R.id.login_button)
        var btnSignup: Button = findViewById<Button>(R.id.signup_button)

        try {
            this.supportActionBar!!.hide()
        } catch (e: NullPointerException) {
        }

        btnLogin.setOnClickListener { view ->
            signIn(view, userbox.text.toString(), passbox.text.toString())
        }

        btnSignup.setOnClickListener { view ->
            signUp(view)
        }

    }

    fun signIn(view: View, email: String, password: String) {
        var passbox: EditText = findViewById<EditText>(R.id.passwordBox)
        var userbox: EditText = findViewById<EditText>(R.id.usernameBox)


        if (userbox.text.toString().length > 0 && passbox.text.toString().length > 0) {
            val text = "Authenticating..."
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, text, duration)
            toast.setGravity(Gravity.TOP , 0, 10)
            toast.show()

            fbAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                if (task.isSuccessful) {
                    var intent = Intent(this, Dashboard::class.java)
                    intent.putExtra("id", fbAuth.currentUser?.email)
                    startActivity(intent)

                } else {
                    val text = "Error: ${task.exception?.message}"
                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.setGravity(Gravity.TOP , 0, 10)
                    toast.show()

                }
            })
        } else {
            val text = "Check Username & Password"
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, text, duration)
            toast.setGravity(Gravity.TOP , 0, 10)
            toast.show()

        }



    }

    fun signUp(view: View) {

        val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
    }

    fun showMessage(view: View, message: String) {
        Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE).setAction("Action", null).show()
    }


}