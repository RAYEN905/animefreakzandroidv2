package geeksdobyte.com.animefreakz

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Gravity
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.auth.FirebaseUser






class RegisterActivity : AppCompatActivity() {
    lateinit var mref: DatabaseReference
    var fbAuth = FirebaseAuth.getInstance()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        var passbox: EditText = findViewById<EditText>(R.id.passwordBox)
        var userbox: EditText = findViewById<EditText>(R.id.usernameBox)

        mref = FirebaseDatabase.getInstance().reference.child("users")

        var btnSignup: Button = findViewById<Button>(R.id.signup_button)

        try {
            this.supportActionBar!!.hide()
        } catch (e: NullPointerException) {
        }

        btnSignup.setOnClickListener { view ->
            signUp(view, userbox.text.toString(), passbox.text.toString())
        }
    }



    fun signUp(view: View, email: String, password: String) {
        var passbox: EditText = findViewById<EditText>(R.id.passwordBox)
        var userbox: EditText = findViewById<EditText>(R.id.usernameBox)
        var fnamebox: EditText = findViewById<EditText>(R.id.lnameBox)
        var lnameox: EditText = findViewById<EditText>(R.id.fnameBox)
        var groupBox: EditText = findViewById<EditText>(R.id.groupBox)

        val user = userbox.getText().toString().trim()
        val pass = passbox.getText().toString().trim()

        if( !user.isNullOrEmpty() && !pass.isNullOrEmpty()) {

            fbAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(this, OnCompleteListener<AuthResult> { task ->
                if (task.isSuccessful) {
                    val text = "You can login now"
                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.setGravity(Gravity.TOP, 0, 10)
                    toast.show()

                    val childUpdates = HashMap<String, Any>()
                    val user = fbAuth.getCurrentUser()
                    mref.child(user?.uid).push()

                    var newUser :userModel = userModel()
                    newUser.email = email
                    newUser.fName = fnamebox.toString()
                    newUser.lName = lnameox.toString()
                    newUser.group = groupBox.toString()
                    newUser.uid = user!!.uid


                    childUpdates.put(user!!.uid, newUser)

                    mref.updateChildren(childUpdates)

                } else {
                    val text = "Error: ${task.exception?.message}"
                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(applicationContext, text, duration)
                    toast.setGravity(Gravity.TOP, 0, 10)
                    toast.show()
                }
            })
        }
        else {
            val text = "Check Email or Password"
            val duration = Toast.LENGTH_SHORT
            val toast = Toast.makeText(applicationContext, text, duration)
            toast.setGravity(Gravity.TOP, 0, 10)
            toast.show()
        }


    }
}
