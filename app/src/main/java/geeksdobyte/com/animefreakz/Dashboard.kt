package geeksdobyte.com.animefreakz

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.GridView
import com.google.firebase.database.*


class Dashboard : AppCompatActivity() {

    lateinit var myRef: DatabaseReference
    lateinit var dashboardList: MutableList<DashboardItem>
    lateinit var GV: GridView
    var itemList: ArrayList<gridLayout> = ArrayList<gridLayout>()
    var data: ArrayList<gridLayout> = ArrayList<gridLayout>()
    var item_list: ArrayList<gridLayout> = ArrayList<gridLayout>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        myRef = FirebaseDatabase.getInstance().reference.child("dashboard")

        GV = findViewById(R.id.GV)

        val myContent = this


        myRef.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError?) {
                println(p0!!.message)
            }

            override fun onDataChange(p0: DataSnapshot?) {
                if (p0!!.exists()) {
                    val children = p0.children
                    children.forEach {
                        val item: DashboardItem = it.getValue(DashboardItem::class.java)!!
                        item_list.add(gridLayout(getDrawInt(item.name), item.name))
                        val adapter = gridAdapter(myContent, R.layout.griditemlayout, item_list)
                        GV.adapter = adapter
                    }
                }


            }

        })



    }


    fun getDrawInt(name: String): Int {

        if (name.toLowerCase() == "news") {
            return R.drawable.news
        } else if (name.toLowerCase() == "chatroom") {
            return R.drawable.chatroom
        } else if (name.toLowerCase() == "music") {
            return R.drawable.music
        } else if (name.toLowerCase() == "quotes") {
            return R.drawable.quotes
        } else if (name.toLowerCase() == "poll") {
            return R.drawable.poll
        } else if (name.toLowerCase() == "suggestion") {
            return R.drawable.suggestion
        } else if (name.toLowerCase() == "LogOut".toLowerCase()) {
            return R.drawable.logout
        }
        return 0
    }


}
