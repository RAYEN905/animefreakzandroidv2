package geeksdobyte.com.animefreakz

data class DashboardModel(val dashboard: List<DashboardItem>? = null,
                          val quotes: List<QuotesItem>? = null)